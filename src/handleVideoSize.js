import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export const handleVideoSize = (videoWidth, videoHeight) => {
  const ratio = width / videoWidth;
  return {
    height: videoHeight * ratio,
    width,
  };
};
