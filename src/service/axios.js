import React, { useContext, createContext } from 'react';
import axios from 'axios';

const AxiosContext = createContext(axios);

/**
 * @param axios
 * @param children
 * @constructor
 */
export const Provider = ({ axios, children }) => {
  return (
    <AxiosContext.Provider value={axios}>{children}</AxiosContext.Provider>
  );
};

/**
 * @return <Promise> return a promise used token as AxiosInstance.
 */
export function useApiWithToken() {
  const axiosInstance = useContext(AxiosContext);
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer token`;
  return axiosInstance;
}

/**
 * @return <Promise> return a promise as AxiosInstance.
 */
export function useApi() {
  return useContext(AxiosContext);
}
