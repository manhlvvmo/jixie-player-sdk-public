import { Platform, StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const styles = StyleSheet.create({
  preloadingPlaceholder: {
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  thumbnail: {
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  playButton: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    width: 64,
    height: 64,
    borderRadius: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playArrow: {
    color: 'white',
  },
  video:
    Platform.Version >= 24
      ? {}
      : {
          backgroundColor: 'black',
        },
  controls: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 50,
    marginTop: -50,
    flexDirection: 'column',
    alignItems: 'center',
  },
  selectQuality: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 150,
    width: width / 2,
    right: 0,
    bottom: 50,
    flexDirection: 'row',
  },
  width1_4: {
    width: width / 4,
  },
  selectTextHeader: {
    color: '#FFF',
    fontSize: 12,
    paddingLeft: 22.5,
  },
  videoControls: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 10,
    paddingBottom: 5,
    alignItems: 'center',
  },
  playControl: {
    color: 'white',
  },
  backward: {
    color: 'white',
    paddingHorizontal: 5,
  },
  forward: {
    color: 'white',
    padding: 5,
  },
  extraControl: {
    color: 'white',
  },
  seekBar: {
    alignItems: 'center',
    height: 10,
    flexGrow: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  seekBarFullWidth: {
    marginLeft: 0,
    marginRight: 0,
    paddingHorizontal: 0,
    marginTop: -3,
    height: 3,
  },
  seekBarProgress: {
    height: 3,
    backgroundColor: '#F00',
  },
  seekBarKnob: {
    width: 20,
    height: 20,
    marginHorizontal: -8,
    marginVertical: -10,
    borderRadius: 10,
    backgroundColor: '#F00',
    transform: [{ scale: 0.8 }],
    zIndex: 1,
  },
  seekBarBackground: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    height: 3,
  },
  overlayButton: {
    flex: 1,
  },

  activeDurationText: {
    paddingLeft: 2,
    paddingRight: 0,
    paddingBottom: 0,
    paddingTop: 0,
  },
  durationText: {
    color: 'white',
  },
  controlScroll: {
    paddingBottom: 35,
    paddingHorizontal: 15,
    maxHeight: 400,
    backgroundColor: '#FFF',
  },
  qualityControlTitle: {
    fontSize: 24,
    padding: 15,
  },
  qualityDetail: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
  },
  backgroundControl: {
    backgroundColor: '#F8F8F8',
  },
});
