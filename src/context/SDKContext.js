import { createContext, useContext } from 'react';

export const SDKContext = createContext({});

export function useSDK() {
  return useContext(SDKContext);
}
