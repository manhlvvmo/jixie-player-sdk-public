/**
 * initial state of player sdk store.
 * @type {{playbackSpeed: number, isPlaying: boolean, clientId: string, appName: string, isControlsVisible: boolean, isStarted: boolean, sessionId: string, videoQuality: string, sid: string, duration: number, mainAdUnit: {delay: string, id: string}, accountId: string, hasEnded: boolean, accessKey: string, videoList: {currentQualities: *[], videoUris: *[], videoRaws: *[], currentVideoId: number, currentIndex: number}, domain: string, width: number, isSeeking: boolean, progress: number, selectModalIsShow: boolean, pageurl: string, isMuted: boolean}}
 */

const initialState = {
  isStarted: false,
  isPlaying: false,
  isMuted: false,
  isControlsVisible: false,
  hasEnded: false,
  progress: 0,
  width: 411,
  duration: 0,
  isSeeking: false,
  videoList: {
    videoRaws: [],
    currentQualities: [],
    currentIndex: 0,
    currentVideoId: 0,
    videoUris: [],
  },
  nextVideoInfo: {
    active: false,
    videoQuality: {},
    videoQualities: [],
  },
  currentVideoInfo: {
    videoQuality: { label: '720p', width: 1280, height: 720 },
    videoQualities: [],
  },
  selectModalIsShow: false,
  appName: 'App_Name',
  accountId: '',
  mainAdUnit: {
    id: '',
    delay: '',
  },
  metadata: {},
  playbackSpeed: 1,
  accessKey: '',
  clientId: '',
  sessionId: '',
  sid: '1641863716-52471830-e2f4-11ea-b5e9-f301ddda9414',
  domain: 'megapolitan.kompas.com',
  pageurl:
    'https://megapolitan.kompas.com/read/2021/05/21/06000081/ppdb-2021--ini-syarat-masuk-dan-proses-dan-jadwal-jenjang-smp-di-jakarta',
};

export default initialState;
