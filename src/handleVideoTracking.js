export const handleTrackVideoProgress = (event, tracker, globalParams) => {
  const _currentTime = Math.round(event.currentTime * 1000);
  const videoPlayPc = (event.currentTime / event.seekableDuration) * 100;
  if (Math.round(videoPlayPc) >= 25 && Math.round(videoPlayPc) < 50) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_25pct',
    };
    return tracker(params);
  }
  if (Math.round(videoPlayPc) >= 50 && Math.round(videoPlayPc) < 75) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_50pct',
    };
    return tracker(params);
  }
  if (Math.round(videoPlayPc) >= 75 && Math.round(videoPlayPc) < 90) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_75pct',
    };
    return tracker(params);
  }
  if (Math.round(videoPlayPc) >= 90 && Math.round(videoPlayPc) < 100) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_90pct',
    };
    return tracker(params);
  }
  if (Math.round(videoPlayPc) === 100) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_100pct',
    };
    return tracker(params);
  }
  if (Math.round(_currentTime / 1000) === 2) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_2s',
    };
    return tracker(params);
  }
  if (Math.round(_currentTime / 1000) === 5) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_5s',
    };
    return tracker(params);
  }
  if (Math.round(_currentTime / 1000) === 15) {
    const params = {
      ...globalParams,
      elapsedms: _currentTime,
      action: 'play_15s',
    };
    return tracker(params);
  }
};
