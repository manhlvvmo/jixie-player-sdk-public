import { styles } from '../Styles';
import React, { Component } from 'react';
import { Image, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const BackgroundImage = ImageBackground || Image; // fall back to Image if RN < 0.46

export default class Thumbnail extends Component {
  state = {
    width: 200,
    height: 200,
  };

  getSizeStyles() {
    const { videoWidth, videoHeight } = this.props;
    const { width } = this.state;
    const ratio = videoHeight / videoWidth;
    return {
      height: width * ratio,
      width,
    };
  }

  render() {
    const { onStartPress, style, customStyles, ...props } = this.props;

    return (
      <BackgroundImage
        {...props}
        style={[
          styles.thumbnail,
          this.getSizeStyles(),
          style,
          customStyles.thumbnail,
        ]}
        source={this.props.thumbnail}
      >
        <TouchableOpacity
          style={[styles.playButton, customStyles.playButton]}
          onPress={onStartPress}
        >
          <Icon
            style={[styles.playArrow, customStyles.playArrow]}
            name="play-arrow"
            size={42}
          />
        </TouchableOpacity>
      </BackgroundImage>
    );
  }
}
