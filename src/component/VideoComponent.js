import React, { useRef, useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import Video, { DRMType } from 'react-native-video-inc-ads';
import { styles } from '../Styles';
import SeekBar from './SeekBar';
import { getDurationTime } from '../handleTime';
import { useSDK } from '../context/SDKContext';
import { handleVideoSize } from '../handleVideoSize';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import SelectView from './SelectView';

/**
 * @description the main video playing component with basic action controls.
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
const VideoComponent = (props) => {
  const { state, dispatch } = useSDK();
  const timeRef = useRef(null);
  const playerRef = useRef(null);
  const [seekTouchStart, setSeekTouchStart] = useState(0);
  const [seekProgressStart, setSeekProgressStart] = useState(0);
  const [seekBarWidth, setSeekBarWidth] = useState(200);
  const [wasPlayingBeforeSeek, setWasPlayingBeforeSeek] = useState(false);
  const [isAdPlay, setIsAsPlay] = useState(false);

  /**
   * get seek UI property
   * @param nativeEvent
   */
  const onSeekBarLayout = (nativeEvent) => {
    const padding = 20;
    setSeekBarWidth(nativeEvent.layout.width - padding);
  };

  /**
   * handle seek finish
   * @param e
   */
  const onSeekGrant = (e) => {
    setSeekTouchStart(e.nativeEvent.pageX);
    setSeekProgressStart(state.progress);
    setWasPlayingBeforeSeek(state.isPlaying);
    dispatch({ type: 'ON_SEEK_GRANT', isSeeking: true, isPlaying: false });
  };

  /**
   * handle seek
   * @param e
   */
  const onSeek = (e) => {
    const diff = e.nativeEvent.pageX - seekTouchStart;
    const ratio = 100 / seekBarWidth;
    const progress = seekProgressStart + (ratio * diff) / 100;
    dispatch({ type: 'ON_SEEK', progress: progress });
    playerRef.current.seek(progress * state.duration);
  };

  /**
   * handle seek 15s
   * @param time
   */
  const onSeek15s = (time) => {
    const { progress, duration } = state;
    dispatch({ type: 'ON_SEEK', progress: progress });
    playerRef.current.seek(duration * progress + time);
  };

  /**
   * handle seek release
   */
  const onSeekRelease = () => {
    dispatch({
      type: 'ON_SEEK_RELEASE',
      isSeeking: false,
      isPlaying: wasPlayingBeforeSeek,
    });
    props.showControls();
  };

  /**
   * sync video playing progress,
   * prepare next video
   * @param event
   */
  function onProgress(event) {
    if (isAdPlay) {
      return;
    }
    if (state.isSeeking) {
      return;
    }
    if (props.onProgress) {
      props.onProgress(event);
    }
    dispatch({
      type: 'ON_PROGRESS',
      progress: event.currentTime / (props.duration || state.duration),
    });
    timeRef.current?.setNativeProps({
      text: getDurationTime(event.currentTime),
    });
  }

  /**
   * handle seek icon to start point
   * @param event
   */
  const onEnd = (event) => {
    if (props.onEnd) {
      props.onEnd(event);
    }
    dispatch({ type: 'ON_END', progress: 1 });

    if (!props.loop) {
      playerRef && playerRef.current.seek(0);
    } else {
      playerRef.current.seek(0);
    }

    timeRef.current?.setNativeProps({
      text: getDurationTime(state.duration),
    });
  };

  /**
   * handle load event
   * @param event
   */
  const onLoad = (event) => {
    if (props.onLoad) {
      props.onLoad(event);
    }
    const { duration } = event;
    dispatch({ type: 'ON_LOAD', duration: duration });
  };

  /**
   * handle mute action
   */
  const onMutePress = () => {
    const isMuted = !state.isMuted;
    if (props.onMutePress) {
      props.onMutePress(isMuted);
    }
    dispatch({ type: 'ON_MUTE', isMuted: isMuted });
    props.showControls();
  };

  /**
   * handle play action
   */
  const onPlayPress = () => {
    if (props.onPlayPress) {
      props.onPlayPress();
    }
    dispatch({ type: 'ON_PLAY', isPlaying: !state.isPlaying });
    props.showControls();
  };

  /**
   * handle update time on time indicator
   * @param e
   */
  const onSeekEvent = (e) => {
    timeRef.current?.setNativeProps({ text: getDurationTime(e.currentTime) });
  };

  /**
   * handle layout display video and VAST
   * @param e
   */
  const adEvent = (e) => {
    switch (e.event) {
      case 'STARTED':
        return setIsAsPlay(true);
      case 'ALL_ADS_COMPLETED':
      case 'COMPLETED':
      case 'SKIPPED':
        return setIsAsPlay(false);
    }
  };

  const { style, pauseOnPress, nextVideo } = props;

  /**
   * @description controls bar
   * @return {JSX.Element}
   */
  const renderControls = () => {
    return (
      <View style={[styles.controls]}>
        <SeekBar
          disableSeek={props.disableSeek}
          fullWidth={false}
          onSeek={onSeek}
          onSeekBarLayout={onSeekBarLayout}
          onSeekGrant={onSeekGrant}
          onSeekRelease={onSeekRelease}
        />
        <View style={styles.videoControls}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={onPlayPress}>
              <Icon
                style={[styles.playControl]}
                name={state.isPlaying ? 'pause' : 'play-arrow'}
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                nextVideo();
              }}
            >
              <Icon style={[styles.playControl]} name={'skip-next'} size={30} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                onSeek15s(-15);
              }}
            >
              <IconFontAwesome
                style={[styles.backward]}
                name={'backward'}
                size={18}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                onSeek15s(15);
              }}
            >
              <IconFontAwesome
                style={[styles.forward]}
                name={'forward'}
                size={18}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                dispatch({
                  type: 'CHANGE_QUALITY_MODAL_STATE',
                  state: !state.selectModalIsShow,
                });
              }}
            >
              <Icon name={'settings'} size={20} color={'#FFF'} />
            </TouchableOpacity>
          </View>

          {props.showDuration && (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <TextInput
                style={[styles.durationText]}
                editable={false}
                ref={timeRef}
                value={getDurationTime(0)}
              />
              <Text style={[styles.durationText]}> / </Text>
              <Text style={[styles.durationText]}>
                {getDurationTime(state.duration)}
              </Text>
            </View>
          )}
          {props.muted ? null : (
            <TouchableOpacity onPress={onMutePress}>
              <Icon
                style={[styles.extraControl]}
                name={state.isMuted ? 'volume-off' : 'volume-up'}
                size={24}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };
  return (
    <View>
      <Video
        onError={(error) => {
          props.onVideoError(error);
        }}
        selectedVideoTrack={{
          type: 'resolution',
          value: state?.currentVideoInfo?.videoQuality?.height,
        }}
        rate={state.playbackSpeed ? state.playbackSpeed : 1}
        adTagUrl={props.adTagUrl}
        onReceiveAdEvent={(event) => {
          props?.onReceiveAdEvent(event);
          adEvent(event);
        }}
        style={[
          styles.video,
          handleVideoSize(
            state?.currentVideoInfo?.videoQuality.width,
            state?.currentVideoInfo?.videoQuality.height
          ),
          style,
          { zIndex: isAdPlay ? 10 : 0 },
        ]}
        ref={playerRef}
        muted={props.muted || state.isMuted}
        paused={!state.isPlaying}
        onProgress={onProgress}
        onEnd={onEnd}
        onLoad={onLoad}
        source={props.source}
        resizeMode={'contain'}
        onSeek={onSeekEvent}
        drm={
          props.drmToken
            ? {
                type: DRMType.WIDEVINE,
                licenseServer:
                  'https://693e0978.drm-widevine-licensing.axprod.net/AcquireLicense',
                headers: {
                  'X-AxDRM-Message': props.drmToken,
                },
              }
            : {}
        }
      />
      <View
        style={[
          handleVideoSize(
            state?.currentVideoInfo?.videoQuality.width,
            state?.currentVideoInfo?.videoQuality.height
          ),
          {
            marginTop: -handleVideoSize(
              state?.currentVideoInfo?.videoQuality.width,
              state?.currentVideoInfo?.videoQuality.height
            ).height,
          },
        ]}
      >
        <TouchableOpacity
          style={styles.overlayButton}
          onPress={() => {
            props.showControls();
            if (pauseOnPress) onPlayPress();
          }}
        />
      </View>
      {!state.isPlaying ||
      state.isControlsVisible ||
      state.selectModalIsShow ? (
        renderControls()
      ) : (
        <SeekBar
          fullWidth={true}
          // customStyles={customStyles}
          disableSeek={props.disableSeek}
          onSeek={onSeek}
          onSeekBarLayout={onSeekBarLayout}
          onSeekGrant={onSeekGrant}
          onSeekRelease={onSeekRelease}
        />
      )}
      {state.selectModalIsShow && <SelectView />}
    </View>
  );
};

export default VideoComponent;
