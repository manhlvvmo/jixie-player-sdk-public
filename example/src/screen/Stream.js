import React, { useRef, useState } from 'react';
import {
  Dimensions,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import VideoPlayer from 'jixie-player-sdk';

const { width } = Dimensions.get('window');
const Stream = ({ route }) => {
  const { videoIds, accountId, unitId, delay } = route.params;
  const [link, setLink] = useState('');
  const [actions, setAction] = useState([]);
  const [event, setEvent] = useState('');
  const [metadata, setMetadata] = useState('');
  const videoRef = useRef(null);
  const flatList = useRef();
  const onEvent = (event) => {
    setEvent(JSON.stringify(event));
  };
  return (
    <View>
      {actions && (
        <FlatList
          ref={flatList}
          onContentSizeChange={() =>
            flatList?.current.scrollToEnd({ animated: true })
          }
          onLayout={() => flatList?.current.scrollToEnd({ animated: true })}
          data={actions}
          renderItem={(element) => {
            return (
              <View style={{ paddingBottom: 10 }} key={`index-${element.item}`}>
                <Text>
                  action: {element.item.action} {element.index}
                </Text>
              </View>
            );
          }}
          keyExtractor={(item) => {
            return item.id;
          }}
          style={{
            width: width - 30,
            height: 250,
            backgroundColor: '#CCC',
            marginHorizontal: 15,
            padding: 10,
            marginTop: 15,
          }}
        />
      )}

      <ScrollView style={{ height: '65%' }} contentInset={{ bottom: 150 }}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              videoRef.current.load(videoIds);
            }}
          >
            <Text>load</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ ...styles.button, width: 90 }}
            onPress={() => {
              videoRef.current.nextVideo();
            }}
          >
            <Text style={{ color: '#212121' }}>next video</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              videoRef.current.play();
            }}
          >
            <Text>play</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              videoRef.current.pause();
            }}
          >
            <Text>pause</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{ ...styles.button, width: 100 }}
          onPress={() => {
            const metadata = videoRef.current.metadata();
            setMetadata(JSON.stringify(metadata));
          }}
        >
          <Text>metadata</Text>
        </TouchableOpacity>
        <VideoPlayer
          ref={videoRef}
          accountId={accountId ? accountId : ''}
          onEvent={onEvent}
          mainAdUnit={{ id: unitId, delay: delay }}
          onGetLink={(link) => {
            setLink(link);
          }}
          onStart={() => {
            const newAction = actions.concat({ action: 'Start' });
            setAction(newAction);
          }}
          onEnded={() => {
            const newAction = actions.concat({ action: 'Ended' });
            setAction(newAction);
          }}
          onVideoChange={() => {
            const newAction = actions.concat({ action: 'VideoChange' });
            setAction(newAction);
          }}
          onPlaying={() => {
            const newAction = actions.concat({ action: 'Playing' });
            setAction(newAction);
          }}
          clientId={'52471830-e2f4-11ea-b5e9-f301ddda9414'}
        />
        <Text style={{ color: '#212121' }}>url: {link}</Text>
        <Text style={{ color: '#212121' }}>metadata: {metadata}</Text>
        <Text style={{ color: '#212121' }}>Event: {event}</Text>
        <View style={{ height: 50 }} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    borderWidth: 0.5,
    borderColor: '#CCC',
    width: 60,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 5,
  },
});

export default Stream;
