/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const Home = ({ navigation }) => {
  const [videoIds, setVideoIds] = useState('16459, 16455, 16451');
  const [delay, setDelay] = useState('5');
  const [accountId, setAccountId] = useState(
    '9262bf2590d558736cac4fff7978fcb1'
  );
  const [unitId, setUnitId] = useState('1000114-qEgXGqRpBy');

  return (
    <ScrollView style={{ paddingTop: 50, backgroundColor: '#FFF' }}>
      <View
        style={{
          width: '90%',
          margin: 20,
        }}
      >
        <Text style={{ color: '#212121' }}>Enter accountId</Text>
        <TextInput
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            padding: 7,
            borderColor: '#CCC',
            marginBottom: 20,
            color: '#212121',
          }}
          onChangeText={(e) => setAccountId(e)}
          value={accountId}
        />
        <Text style={{ color: '#212121' }}>Enter videoIds</Text>
        <TextInput
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            padding: 7,
            borderColor: '#CCC',
            color: '#212121',
          }}
          onChangeText={(e) => {
            setVideoIds(e);
          }}
          value={videoIds}
        />
        <Text style={{ color: '#212121', paddingTop: 20 }}>Enter UnitId</Text>
        <TextInput
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            padding: 7,
            borderColor: '#CCC',
            color: '#212121',
          }}
          onChangeText={(e) => {
            setUnitId(e);
          }}
          value={unitId}
        />
        <Text style={{ color: '#212121', paddingTop: 20 }}>
          Enter delay(second)
        </Text>
        <TextInput
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            padding: 7,
            borderColor: '#CCC',
            color: '#212121',
          }}
          onChangeText={(e) => {
            setDelay(e);
          }}
          value={delay}
        />
      </View>
      <TouchableOpacity
        style={{
          marginLeft: 15,
          borderWidth: 0.5,
          borderColor: '#CCC',
          width: 100,
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: 10,
        }}
        onPress={() => {
          const videoArr = videoIds.split(',');
          navigation.navigate('Stream', {
            videoIds: videoArr,
            accountId: accountId,
            unitId: unitId,
            delay: delay,
          });
        }}
      >
        <Text style={{ color: '#212121' }}>Test</Text>
      </TouchableOpacity>
      <View style={{ height: 100 }} />
    </ScrollView>
  );
};

export default Home;
