"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

var _axios = require("../service/axios");

/**
 * @return {[{stream: {}, pending: boolean, error: {}}, {getVideo: ((function(): void)|*)}]}
 */
const useStream = () => {
  const [pending, setPending] = (0, _react.useState)(false);
  const [stream, setStream] = (0, _react.useState)({});
  const [error, setError] = (0, _react.useState)({});
  const axios = (0, _axios.useApi)();
  const getVideo = (0, _react.useCallback)(url => {
    setPending(true);
    axios.get(url).then(result => {
      setStream(result.data);
    }).catch(error => {
      setError(error === null || error === void 0 ? void 0 : error.response);
    }).finally(() => {
      setPending(false);
    });
  }, [axios]);
  return [{
    stream,
    error,
    pending
  }, {
    getVideo
  }];
};

var _default = useStream;
exports.default = _default;
//# sourceMappingURL=useStream.js.map