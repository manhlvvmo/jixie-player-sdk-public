"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SDKContext = void 0;
exports.useSDK = useSDK;

var _react = require("react");

const SDKContext = /*#__PURE__*/(0, _react.createContext)({});
exports.SDKContext = SDKContext;

function useSDK() {
  return (0, _react.useContext)(SDKContext);
}
//# sourceMappingURL=SDKContext.js.map