"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleAdsTrackingParams = void 0;

/**
 * handle param before tracking
 * @param event
 * @param globalParams
 * @return {(*&{action: string})|*}
 */
const handleAdsTrackingParams = (event, globalParams) => {
  switch (event) {
    case 'STARTED':
      return { ...globalParams,
        action: 'adplay'
      };

    case 'ALL_ADS_COMPLETED':
      return { ...globalParams,
        action: 'adslotend'
      };

    case 'ERROR':
      return { ...globalParams,
        action: 'aderror'
      };

    default:
      return globalParams;
  }
};

exports.handleAdsTrackingParams = handleAdsTrackingParams;
//# sourceMappingURL=handleAdsTracking.js.map