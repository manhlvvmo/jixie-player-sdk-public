export const reducer = (prevState, action) => {
  switch (action.type) {
    case 'SET_LAYOUT':
      return { ...prevState,
        width: action.width
      };

    case 'ON_START':
      return { ...prevState,
        isPlaying: action.isPlaying,
        isStarted: action.isStarted,
        hasEnded: action.hasEnded,
        progress: action.progress
      };

    case 'PAUSE':
      return { ...prevState,
        isPlaying: false
      };

    case 'ON_PROGRESS':
      return { ...prevState,
        progress: action.progress
      };

    case 'ON_END':
      return { ...prevState,
        progress: action.progress
      };

    case 'ON_LOOP':
      return { ...prevState,
        isPlaying: action.isPlaying
      };

    case 'ON_LOAD':
      return { ...prevState,
        duration: action.duration
      };

    case 'ON_PLAY':
      return { ...prevState,
        isPlaying: action.isPlaying
      };

    case 'ON_MUTE':
      return { ...prevState,
        isMuted: action.isMuted
      };

    case 'ON_SEEK_GRANT':
      return { ...prevState,
        isSeeking: action.isSeeking,
        isPlaying: action.isPlaying
      };

    case 'ON_SEEK_RELEASE':
      return { ...prevState,
        isSeeking: action.isSeeking,
        isPlaying: action.isPlaying
      };

    case 'ON_SEEK':
      return { ...prevState,
        progress: action.progress
      };

    case 'HIDE_CONTROLS':
      return { ...prevState,
        isControlsVisible: false
      };

    case 'SHOW_CONTROLS':
      return { ...prevState,
        isControlsVisible: true
      };

    case 'SET_VIDEO_RAW':
      const currentVideoId = action.videos ? action.videos[0] : '';

      const _currentVideoId = currentVideoId.includes('https') ? 0 : currentVideoId.trim();

      return { ...prevState,
        videoList: { ...prevState.videoList,
          videoRaws: action.videos || [],
          currentIndex: 0,
          currentVideoId: _currentVideoId
        }
      };

    case 'CHANGE_QUALITY_MODAL_STATE':
      return { ...prevState,
        selectModalIsShow: action.state
      };

    case 'SET_CURRENT_VIDEO_INDEX':
      const videoId = prevState.videoList.videoRaws[action.currentIndex];

      const _videoId = videoId !== null && videoId !== void 0 && videoId.includes('https') ? 0 : videoId === null || videoId === void 0 ? void 0 : videoId.trim();

      return { ...prevState,
        videoList: { ...prevState.videoList,
          currentIndex: action.currentIndex,
          currentVideoId: _videoId
        }
      };

    case 'SET_VIDEO_INFORMATION':
      return { ...prevState,
        videoList: { ...prevState.videoList,
          videoUris: [...prevState.videoList.videoUris, action.videoUri]
        }
      };

    case 'SET_MAIN_AD_UNIT':
      return { ...prevState,
        mainAdUnit: action.mainAdUnit
      };

    case 'SET_ACCOUNT_ID':
      return { ...prevState,
        accountId: action.accountId
      };

    case 'SET_APP_NAME':
      return { ...prevState,
        appName: action.appName
      };

    case 'SET_PAGE_NAME':
      return { ...prevState,
        pageName: action.pageName
      };

    case 'SET_ACCESS_KEY':
      return { ...prevState,
        accessKey: action.accessKey
      };

    case 'SET_VIDEO_QUALITY':
      return { ...prevState,
        currentVideoInfo: { ...prevState.currentVideoInfo,
          videoQuality: action.videoQuality,
          videoQualities: action.videoQualities ? action.videoQualities : prevState.currentVideoInfo.videoQualities
        }
      };

    case 'SYNC_VIDEO_QUALITY':
      return { ...prevState,
        currentVideoInfo: { ...prevState.nextVideoInfo
        },
        nextVideoInfo: {
          active: false
        }
      };

    case 'SET_NEXT_VIDEO_QUALITY':
      return { ...prevState,
        nextVideoInfo: { ...prevState.nextVideoInfo,
          active: true,
          videoQuality: action.videoQuality,
          videoQualities: action.videoQualities
        }
      };

    case 'SET_PLAYBACK_SPEED':
      return { ...prevState,
        playbackSpeed: action.playbackSpeed
      };

    case 'SET_CLIENT_ID':
      return { ...prevState,
        clientId: action.clientId,
        sessionId: `${Date.now()}-${action.clientId}`
      };

    case 'SET_METADATA':
      return { ...prevState,
        metadata: action.metadata
      };

    default:
      return { ...prevState
      };
  }
};
//# sourceMappingURL=SDKReducer.js.map