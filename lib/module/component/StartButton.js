import React from 'react';
import { TouchableOpacity } from 'react-native';
import { styles } from '../Styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
export const StartButton = _ref => {
  let {
    onStartPress
  } = _ref;
  return /*#__PURE__*/React.createElement(TouchableOpacity, {
    style: [styles.playButton],
    onPress: () => onStartPress()
  }, /*#__PURE__*/React.createElement(Icon, {
    style: [styles.playArrow],
    name: "play-arrow",
    size: 42
  }));
};
export default StartButton;
//# sourceMappingURL=StartButton.js.map