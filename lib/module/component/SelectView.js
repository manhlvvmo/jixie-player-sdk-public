import React from 'react';
import { styles } from '../Styles';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useSDK } from '../context/SDKContext';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const SelectView = () => {
  var _state$currentVideoIn, _state$currentVideoIn2;

  const {
    state,
    dispatch
  } = useSDK();
  const videoQualities = (state === null || state === void 0 ? void 0 : (_state$currentVideoIn = state.currentVideoInfo) === null || _state$currentVideoIn === void 0 ? void 0 : _state$currentVideoIn.videoQualities) || [];
  const videoQuality = (state === null || state === void 0 ? void 0 : (_state$currentVideoIn2 = state.currentVideoInfo) === null || _state$currentVideoIn2 === void 0 ? void 0 : _state$currentVideoIn2.videoQuality) || {};
  const playBackSpeed = [{
    label: '0.25',
    value: 0.25
  }, {
    label: '0.5',
    value: 0.5
  }, {
    label: '0.75',
    value: 0.75
  }, {
    label: 'Normal',
    value: 1
  }, {
    label: '1.25',
    value: 1.25
  }, {
    label: '1.5',
    value: 1.5
  }, {
    label: '1.75',
    value: 1.75
  }, {
    label: '2',
    value: 2
  }];
  return /*#__PURE__*/React.createElement(View, {
    style: styles.selectQuality
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.width1_4
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.selectTextHeader
  }, "Qualities"), /*#__PURE__*/React.createElement(ScrollView, null, videoQualities.map(element => {
    return /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        dispatch({
          type: 'SET_VIDEO_QUALITY',
          videoQuality: element
        });
      },
      key: `index${element.label}`,
      style: styles.qualityDetail
    }, element.height === videoQuality.height ? /*#__PURE__*/React.createElement(IconFontAwesome, {
      style: styles.backward,
      name: 'check-circle-o',
      size: 14
    }) : /*#__PURE__*/React.createElement(View, {
      style: {
        width: 22.5
      }
    }), /*#__PURE__*/React.createElement(Text, {
      style: {
        fontSize: 12,
        color: '#FFF'
      }
    }, element.label));
  }))), /*#__PURE__*/React.createElement(View, {
    style: styles.width1_4
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.selectTextHeader
  }, "Speed"), /*#__PURE__*/React.createElement(ScrollView, null, playBackSpeed.map(element => {
    return /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        dispatch({
          type: 'SET_PLAYBACK_SPEED',
          playbackSpeed: element.value
        });
      },
      key: `index${element.value}`,
      style: styles.qualityDetail
    }, element.value === state.playbackSpeed ? /*#__PURE__*/React.createElement(IconFontAwesome, {
      style: styles.backward,
      name: 'check-circle-o',
      size: 14
    }) : /*#__PURE__*/React.createElement(View, {
      style: {
        width: 22.5
      }
    }), /*#__PURE__*/React.createElement(Text, {
      style: {
        fontSize: 12,
        color: '#FFF'
      }
    }, element.label));
  }))));
};

export default SelectView;
//# sourceMappingURL=SelectView.js.map