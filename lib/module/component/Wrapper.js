import React, { useEffect, useImperativeHandle, useState } from 'react';
import { Dimensions, ImageBackground, Platform, View } from 'react-native';
import { styles } from '../Styles';
import { handleVideoSize } from '../handleVideoSize';
import StartButton from './StartButton';
import VideoComponent from './VideoComponent';
import { useSDK } from '../context/SDKContext';
import useStream from '../hooks/useStream';
import useTrack from '../hooks/useTrack';
import { handleTrackVideoProgress } from '../handleVideoTracking';
import { handleAdsTrackingParams } from '../handleAdsTracking';
import { handleRendition } from '../handleRendition';
import { handleBuildAdTagUrl } from '../handleBuildAdTagUrl';
import { renditionLimit } from '../renditionLimit';
const initTracking = {
  adslot: true,
  play_25pct: true,
  play_50pct: true,
  play_75pct: true,
  play_90pct: true,
  play_100pct: true
};
const NEXT_VIDEO_TIME = 10;
/**
 * @description the wrapper of video component with the integration of APIs and event tracking
 * @param onStartPress
 * @param showControls
 * @param showDuration
 * @param onGetLink
 * @param onEvent
 * @param onGetAdTagUrl
 * @param currentVideoIndex
 * @return {JSX.Element}
 * @constructor
 */

const Wrapper = (_ref, ref) => {
  var _stream$data, _state$videoList, _state$videoList2, _state$videoList3, _state$videoList4, _state$videoList4$vid, _stream$data2, _stream$data3, _stream$data4, _state$videoList5, _state$videoList5$vid, _state$videoList7, _state$currentVideoIn2, _state$currentVideoIn3, _stream$data14, _stream$data14$metada, _stream$data15, _stream$data16, _state$currentVideoIn6, _state$currentVideoIn7, _state$currentVideoIn8, _state$currentVideoIn9;

  let {
    onStartPress,
    showControls,
    showDuration,
    onGetLink,
    onEvent,
    onGetAdTagUrl,
    onEnded,
    onVideoChange,
    onPlaying
  } = _ref;
  const {
    width,
    height
  } = Dimensions.get('window');

  const _height = Math.round(height);

  const _width = Math.round(width);

  const {
    state,
    dispatch
  } = useSDK();
  const [{
    stream,
    error: getStreamsError,
    pending
  }, {
    getVideo
  }] = useStream();
  const {
    isStarted
  } = state;
  const streams = stream === null || stream === void 0 ? void 0 : (_stream$data = stream.data) === null || _stream$data === void 0 ? void 0 : _stream$data.streams;
  const [uri, setUri] = useState('');
  const [holdUri, setHoldUri] = useState(false);
  const [clean, setClean] = useState(false);
  const [adTagUrl, setAdTagUrl] = useState('');
  const [rendition, setRendition] = useState('');
  const [currentTime, setCurrentTime] = useState(0);
  const [aggregateTime, setAggregateTime] = useState(0);
  const [trackState, setTrackingState] = useState(initTracking);
  const videoUris = state === null || state === void 0 ? void 0 : (_state$videoList = state.videoList) === null || _state$videoList === void 0 ? void 0 : _state$videoList.videoUris;
  const currentIndex0fStore = state === null || state === void 0 ? void 0 : (_state$videoList2 = state.videoList) === null || _state$videoList2 === void 0 ? void 0 : _state$videoList2.currentIndex;
  const videoRaws = state === null || state === void 0 ? void 0 : (_state$videoList3 = state.videoList) === null || _state$videoList3 === void 0 ? void 0 : _state$videoList3.videoRaws;
  const accountId = state === null || state === void 0 ? void 0 : state.accountId;
  useImperativeHandle(ref, () => ({
    nextVideo: () => {
      setCurrentTime(0);
      nextVideo();
    }
  }));
  /**
   * check video link
   * @param video
   * @return {*}
   * @constructor
   */

  const IsVideoLink = video => {
    return video && video.includes('https');
  };

  const _currentTimeMs = Math.round(currentTime * 1000);
  /**
   * @description default tracking params for all events
   * @type {{origtech: string, vposition: (number|*), viewability: number, os: "android" | "ios" | "windows" | "macos" | "web", startmode: string, adTagUrl: string, session_id: *, videoid: (*|number), ownerid: (*|string), autoplay: boolean, client_id: *, volume: (number), playhead: number, accountid, rendition: string, p_domain: *, segment: (*|string), domain: *, browser: *, action: string, pageurl: (string|*), realtech: string, device: string, elapsedms: number}}
   */


  const globalParams = {
    action: '',
    videoid: (state === null || state === void 0 ? void 0 : (_state$videoList4 = state.videoList) === null || _state$videoList4 === void 0 ? void 0 : (_state$videoList4$vid = _state$videoList4.videoRaws) === null || _state$videoList4$vid === void 0 ? void 0 : _state$videoList4$vid[currentIndex0fStore]) || 0,
    elapsedms: _currentTimeMs,
    vposition: currentIndex0fStore,
    autoplay: false,
    startmode: 'click',
    volume: state.isMuted ? 0 : 100,
    playhead: Math.round(currentTime),
    rendition: rendition,
    viewability: 100,
    segment: (stream === null || stream === void 0 ? void 0 : (_stream$data2 = stream.data) === null || _stream$data2 === void 0 ? void 0 : _stream$data2.segment) || '',
    origtech: `${Platform.OS}sdk`,
    realtech: `${Platform.OS}sdk`,
    accountid: state.accountId,
    ownerid: (stream === null || stream === void 0 ? void 0 : (_stream$data3 = stream.data) === null || _stream$data3 === void 0 ? void 0 : _stream$data3.owner_id) || '',
    device: 'mobile',
    p_domain: state === null || state === void 0 ? void 0 : state.appName,
    domain: state === null || state === void 0 ? void 0 : state.domain,
    pageurl: state === null || state === void 0 ? void 0 : state.pageurl,
    browser: state === null || state === void 0 ? void 0 : state.appName,
    os: Platform.OS,
    client_id: state === null || state === void 0 ? void 0 : state.clientId,
    session_id: state === null || state === void 0 ? void 0 : state.sessionId,
    adTagUrl: adTagUrl
  };
  /**
   * @description tracking params for duragg event
   * @type {{accountid: *, rendition: string, action: string, videoid: (*|number), step: number, ownerid: (*|string)}}
   */

  const aggregateParams = {
    action: 'duragg',
    accountid: state === null || state === void 0 ? void 0 : state.accountId,
    ownerid: (stream === null || stream === void 0 ? void 0 : (_stream$data4 = stream.data) === null || _stream$data4 === void 0 ? void 0 : _stream$data4.owner_id) || '',
    videoid: (state === null || state === void 0 ? void 0 : (_state$videoList5 = state.videoList) === null || _state$videoList5 === void 0 ? void 0 : (_state$videoList5$vid = _state$videoList5.videoRaws) === null || _state$videoList5$vid === void 0 ? void 0 : _state$videoList5$vid[currentIndex0fStore]) || 0,
    rendition: rendition,
    step: 5
  };
  const [{}, {
    tracker
  }] = useTrack();
  /**
   * @description handle tracking events
   * @param params
   */

  const handleTracking = params => {
    /**
     * make sure send only one record for each event
     */
    if (params.action === 'play_25pct' || params.action === 'play_50pct' || params.action === 'play_75pct' || params.action === 'play_90pct' || params.action === 'play_100pct') {
      if (trackState[params.action]) {
        tracker(params);
        onEvent && onEvent(params);
      }

      return setTrackingState({ ...trackState,
        [params.action]: false
      });
    }

    onEvent && onEvent(params);
    return tracker(params);
  };
  /**
   * tracking error when API calls fail
   */


  useEffect(() => {
    var _getStreamsError$data;

    const getStreamsErrorParams = { ...globalParams,
      action: 'error',
      errormessage: getStreamsError === null || getStreamsError === void 0 ? void 0 : (_getStreamsError$data = getStreamsError.data) === null || _getStreamsError$data === void 0 ? void 0 : _getStreamsError$data.message,
      errorcode: `90${getStreamsError === null || getStreamsError === void 0 ? void 0 : getStreamsError.status}`
    };

    if (getStreamsError !== null && getStreamsError !== void 0 && getStreamsError.status) {
      streams && onEvent && onEvent(getStreamsErrorParams);
      tracker(getStreamsErrorParams);
    }
  }, [getStreamsError]);
  /**
   * get url when videoid
   */

  useEffect(() => {
    var _state$videoList6;

    (state === null || state === void 0 ? void 0 : (_state$videoList6 = state.videoList) === null || _state$videoList6 === void 0 ? void 0 : _state$videoList6.videoRaws.length) && handleVideoRaw(videoRaws, 0, 99);
  }, [state === null || state === void 0 ? void 0 : (_state$videoList7 = state.videoList) === null || _state$videoList7 === void 0 ? void 0 : _state$videoList7.videoRaws]);
  /**
   * set adTagUrl to state component
   */

  useEffect(() => {
    var _stream$data5, _stream$data6, _stream$data6$conf, _stream$data7, _stream$data7$metadat;

    const conf = stream !== null && stream !== void 0 && (_stream$data5 = stream.data) !== null && _stream$data5 !== void 0 && _stream$data5.conf ? stream === null || stream === void 0 ? void 0 : (_stream$data6 = stream.data) === null || _stream$data6 === void 0 ? void 0 : (_stream$data6$conf = _stream$data6.conf) === null || _stream$data6$conf === void 0 ? void 0 : _stream$data6$conf.ads : {};
    const duration = stream === null || stream === void 0 ? void 0 : (_stream$data7 = stream.data) === null || _stream$data7 === void 0 ? void 0 : (_stream$data7$metadat = _stream$data7.metadata) === null || _stream$data7$metadat === void 0 ? void 0 : _stream$data7$metadat.duration;

    const _adTagUrl = state ? handleBuildAdTagUrl(state, conf, duration) : '';

    if (onGetAdTagUrl) {
      onGetAdTagUrl(_adTagUrl);
    }

    setAdTagUrl(_adTagUrl);
  }, [state.mainAdUnit, streams]);
  /**
   * fire {loaded} event
   */

  useEffect(() => {
    const params = { ...globalParams,
      action: 'loaded',
      videoid: 0
    };
    onEvent && onEvent(params);
    tracker(params);
  }, []);
  /**
   * set response from stream API to store and state
   */

  useEffect(() => {
    var _stream$data8, _stream$data9;

    const qualities = stream !== null && stream !== void 0 && (_stream$data8 = stream.data) !== null && _stream$data8 !== void 0 && _stream$data8.qualities ? stream === null || stream === void 0 ? void 0 : (_stream$data9 = stream.data) === null || _stream$data9 === void 0 ? void 0 : _stream$data9.qualities : [];

    if (streams) {
      var _stream$data10, _stream$data11, _stream$data12, _stream$data13;

      onGetLink(handleStreamData(streams, 'HLS'));
      dispatch({
        type: 'SET_VIDEO_INFORMATION',
        videoUri: handleStreamData(streams, 'HLS')
      });
      dispatch({
        type: holdUri ? 'SET_NEXT_VIDEO_QUALITY' : 'SET_VIDEO_QUALITY',
        videoQualities: renditionLimit(stream === null || stream === void 0 ? void 0 : (_stream$data10 = stream.data) === null || _stream$data10 === void 0 ? void 0 : _stream$data10.ratio, qualities),
        videoQuality: handleRendition(handleStreamData(streams, 'HLS'), renditionLimit(stream === null || stream === void 0 ? void 0 : (_stream$data11 = stream.data) === null || _stream$data11 === void 0 ? void 0 : _stream$data11.ratio, qualities)).quality
      });
      const params = { ...globalParams,
        action: 'ready',
        rendition: streams ? handleRendition(handleStreamData(streams, 'HLS'), renditionLimit(stream === null || stream === void 0 ? void 0 : (_stream$data12 = stream.data) === null || _stream$data12 === void 0 ? void 0 : _stream$data12.ratio, qualities)).rendition : ''
      };
      dispatch({
        type: 'SET_METADATA',
        metadata: stream === null || stream === void 0 ? void 0 : (_stream$data13 = stream.data) === null || _stream$data13 === void 0 ? void 0 : _stream$data13.metadata
      });
      onEvent && tracker(params);
      tracker(params);
    }
  }, [streams]);
  /**
   * update url of current video index
   */

  useEffect(() => {
    const _uri = videoUris[currentIndex0fStore];

    if (!holdUri) {
      setUri(_uri);
    }
  }, [videoUris]);
  /**
   * update rendition
   */

  useEffect(() => {
    var _state$currentVideoIn;

    const quality = state === null || state === void 0 ? void 0 : (_state$currentVideoIn = state.currentVideoInfo) === null || _state$currentVideoIn === void 0 ? void 0 : _state$currentVideoIn.videoQuality;
    setRendition(`${quality.width}x${quality.height}`);
  }, [state === null || state === void 0 ? void 0 : (_state$currentVideoIn2 = state.currentVideoInfo) === null || _state$currentVideoIn2 === void 0 ? void 0 : (_state$currentVideoIn3 = _state$currentVideoIn2.videoQuality) === null || _state$currentVideoIn3 === void 0 ? void 0 : _state$currentVideoIn3.label]);
  /**
   * handle next video action
   */

  const nextVideo = () => {
    if (onVideoChange) {
      onVideoChange();
    }

    const index = currentIndex0fStore < videoRaws.length - 1 ? currentIndex0fStore + 1 : videoRaws.length - 1;
    const params = { ...globalParams,
      action: 'videochange'
    };

    if (onEvent) {
      onEvent(params);
    }

    dispatch({
      type: 'SET_CURRENT_VIDEO_INDEX',
      currentIndex: index
    });
    setTrackingState(initTracking);
    setAggregateTime(0);

    if (!holdUri) {
      handleVideoRaw(videoRaws, index, 198);
    } else {
      const _uri = videoUris[index];
      setHoldUri(false);
      setUri(_uri);
    }

    handleCleanPrevVideo();
    dispatch({
      type: 'ON_START',
      isPlaying: true,
      isStarted: true,
      hasEnded: false,
      progress: state.progress === 1 ? 0 : state.progress
    }); // setHoldUri(false);
  };
  /**
   * handle cleaning of previous video
   */


  const handleCleanPrevVideo = () => {
    setClean(true);
    setImmediate(() => {
      setClean(false);
    });
  };
  /**
   * @description handle get video url
   * @param videos
   * @param index
   * @param line
   */


  const handleVideoRaw = (videos, index, line) => {
    const video = videos[index] ? videos[index].trim() : '';

    if (IsVideoLink(video)) {
      onGetLink(video);
      dispatch({
        type: 'SET_VIDEO_INFORMATION',
        qualities: [],
        videoUri: video,
        index: index,
        line: line
      });
    }

    return getVideoUrl(video);
  };
  /**
   * handle start action
   * @private
   */


  const _onStartPress = () => {
    const params = { ...globalParams,
      action: 'start',
      playhead: 0
    };
    const playParams = { ...globalParams,
      action: 'play',
      step: 5
    };

    if (onEvent) {
      onEvent(params);
    }

    tracker(params);
    tracker(playParams);
    onStartPress();
  };
  /**
   * handle play/pause action
   */


  const onPlayPress = () => {
    const params = { ...globalParams,
      action: !state.isPlaying ? 'play' : 'pause'
    };

    if (onEvent) {
      onEvent(params);
    }

    tracker(params);
  };
  /**
   * handle playing event
   * @param event
   */


  const onProgress = event => {
    const _currentTime = Math.round(event.currentTime);

    const params = { ...globalParams,
      action: 'playing'
    };
    const delay = state.mainAdUnit.delay ? state.mainAdUnit.delay * 1000 : 5000;
    const adslotParams = { ...globalParams,
      action: 'adslot',
      elapsedms: Math.round(event.currentTime * 1000)
    };

    if (Math.round(currentTime) <= _currentTime - 1) {
      handleTrackVideoProgress(event, handleTracking, globalParams);
      setCurrentTime(event.currentTime);
      onEvent && onEvent(params);
      onPlaying && onPlaying();

      if (delay === _currentTime * 1000) {
        trackState.adslot && tracker(adslotParams);
        setTrackingState({ ...trackState,
          adslot: false
        });
      }

      if (NEXT_VIDEO_TIME === Math.round(event.seekableDuration - event.currentTime)) {
        onPrepareNextVideo();
      }
    }

    if (Math.round(aggregateTime) <= _currentTime - 5) {
      setAggregateTime(_currentTime);
      const _aggregateParams = { ...aggregateParams,
        step: state.duration - event.currentTime < 5 ? Math.floor(state.duration - event.currentTime) : 5
      };
      /**
       * tracking duragg event
       */

      tracker(_aggregateParams, true);
    }
  };
  /**
   * handle ad tracking
   * @param e
   */


  const onReceiveAdEvent = e => {
    const params = handleAdsTrackingParams(e.event, globalParams);
    params.action && onEvent && onEvent(params);
    params.action && tracker(params);
  };
  /**
   * handle video error tracking
   * @param error
   */


  const onVideoError = error => {
    const errorString = JSON.stringify(error.error);
    const params = { ...globalParams,
      action: 'error',
      errormessage: errorString
    };
    onEvent && onEvent(params);
    error && tracker(params);
  };
  /**
   * handle muted tracking
   * @param isMuted
   */


  const onMutePress = isMuted => {
    const params = { ...globalParams,
      action: 'volume',
      volume: isMuted ? 0 : 100
    };
    onEvent && onEvent(params);
    tracker(params);
  };
  /**
   * handle ended video
   */


  const onEnd = () => {
    const params = { ...globalParams,
      action: 'ended'
    };

    if (onEvent) {
      onEvent(params);
    }

    if (onEnded) {
      onEnded();
    }

    dispatch({
      type: 'ON_PLAY',
      isPlaying: currentIndex0fStore + 1 < videoRaws.length
    });
    state.nextVideoInfo.active && dispatch({
      type: 'SYNC_VIDEO_QUALITY'
    });
    const _uri = videoUris[currentIndex0fStore + 1];
    handleCleanPrevVideo();
    setTrackingState(initTracking);
    _uri && setUri(_uri);
    setHoldUri(false);
  };
  /**
   * use hook call stream API
   * @param videoId
   */


  const getVideoUrl = videoId => {
    const videoParam = {
      height: _height,
      width: _width,
      format: 'hls',
      metadata: 'basic'
    };
    const _formatPath = `format=${videoParam.format}`;
    const _metadataPath = `&metadata=${videoParam.metadata}`;
    const _heightPath = `&max-height=${videoParam.height}`;
    const _widthPath = `&max-height=${videoParam.width}`;
    const _videoIdPath = `&video_id=${videoId}`;
    const _accountIdPath = `&conf=${accountId}`;
    const url = `api/public/stream?` + _formatPath + _metadataPath + _heightPath + _widthPath + _videoIdPath + _accountIdPath;
    return getVideo(url);
  };
  /**
   * handle call stream API before next video automatic.
   */


  const onPrepareNextVideo = () => {
    const index = currentIndex0fStore + 1;

    if (index < videoRaws.length) {
      setHoldUri(true);
      getVideoUrl(videoRaws[index]);
    }
  };

  const onLoad = event => {};
  /**
   * handle streams data
   * @param streams
   * @param type
   * @return {*|string}
   */


  function handleStreamData(streams, type) {
    const stream = streams.find(stream => {
      return stream.type === type;
    });
    return stream ? stream.url : '';
  }

  const thumbnail = stream === null || stream === void 0 ? void 0 : (_stream$data14 = stream.data) === null || _stream$data14 === void 0 ? void 0 : (_stream$data14$metada = _stream$data14.metadata) === null || _stream$data14$metada === void 0 ? void 0 : _stream$data14$metada.thumbnail;
  /**
   * handle show thumbnail
   */

  if (pending && !holdUri || clean || !uri) {
    var _state$currentVideoIn4, _state$currentVideoIn5;

    return /*#__PURE__*/React.createElement(View, {
      style: { ...handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn4 = state.currentVideoInfo) === null || _state$currentVideoIn4 === void 0 ? void 0 : _state$currentVideoIn4.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn5 = state.currentVideoInfo) === null || _state$currentVideoIn5 === void 0 ? void 0 : _state$currentVideoIn5.videoQuality.height),
        backgroundColor: 'black'
      }
    });
  }

  return /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(VideoComponent, {
    onVideoError: onVideoError,
    showControls: showControls,
    source: {
      uri: uri
    },
    showDuration: showDuration,
    nextVideo: () => {
      return nextVideo();
    },
    onEnd: onEnd,
    onPlayPress: onPlayPress,
    onProgress: onProgress,
    adTagUrl: adTagUrl,
    onReceiveAdEvent: onReceiveAdEvent,
    onMutePress: onMutePress,
    drm: stream === null || stream === void 0 ? void 0 : (_stream$data15 = stream.data) === null || _stream$data15 === void 0 ? void 0 : _stream$data15.drm,
    drmToken: stream === null || stream === void 0 ? void 0 : (_stream$data16 = stream.data) === null || _stream$data16 === void 0 ? void 0 : _stream$data16.license_token,
    onLoad: onLoad
  }), !isStarted && /*#__PURE__*/React.createElement(View, {
    style: [styles.preloadingPlaceholder, handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn6 = state.currentVideoInfo) === null || _state$currentVideoIn6 === void 0 ? void 0 : _state$currentVideoIn6.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn7 = state.currentVideoInfo) === null || _state$currentVideoIn7 === void 0 ? void 0 : _state$currentVideoIn7.videoQuality.height)]
  }, /*#__PURE__*/React.createElement(ImageBackground, {
    style: {
      justifyContent: 'center',
      alignItems: 'center',
      ...handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn8 = state.currentVideoInfo) === null || _state$currentVideoIn8 === void 0 ? void 0 : _state$currentVideoIn8.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn9 = state.currentVideoInfo) === null || _state$currentVideoIn9 === void 0 ? void 0 : _state$currentVideoIn9.videoQuality.height)
    },
    source: {
      uri: thumbnail
    }
  }, /*#__PURE__*/React.createElement(StartButton, {
    onStartPress: _onStartPress
  }))));
};

export default /*#__PURE__*/React.forwardRef(Wrapper);
//# sourceMappingURL=Wrapper.js.map