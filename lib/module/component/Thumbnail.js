function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { styles } from '../Styles';
import React, { Component } from 'react';
import { Image, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
const BackgroundImage = ImageBackground || Image; // fall back to Image if RN < 0.46

export default class Thumbnail extends Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      width: 200,
      height: 200
    });
  }

  getSizeStyles() {
    const {
      videoWidth,
      videoHeight
    } = this.props;
    const {
      width
    } = this.state;
    const ratio = videoHeight / videoWidth;
    return {
      height: width * ratio,
      width
    };
  }

  render() {
    const {
      onStartPress,
      style,
      customStyles,
      ...props
    } = this.props;
    return /*#__PURE__*/React.createElement(BackgroundImage, _extends({}, props, {
      style: [styles.thumbnail, this.getSizeStyles(), style, customStyles.thumbnail],
      source: this.props.thumbnail
    }), /*#__PURE__*/React.createElement(TouchableOpacity, {
      style: [styles.playButton, customStyles.playButton],
      onPress: onStartPress
    }, /*#__PURE__*/React.createElement(Icon, {
      style: [styles.playArrow, customStyles.playArrow],
      name: "play-arrow",
      size: 42
    })));
  }

}
//# sourceMappingURL=Thumbnail.js.map