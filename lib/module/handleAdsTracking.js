/**
 * handle param before tracking
 * @param event
 * @param globalParams
 * @return {(*&{action: string})|*}
 */
export const handleAdsTrackingParams = (event, globalParams) => {
  switch (event) {
    case 'STARTED':
      return { ...globalParams,
        action: 'adplay'
      };

    case 'ALL_ADS_COMPLETED':
      return { ...globalParams,
        action: 'adslotend'
      };

    case 'ERROR':
      return { ...globalParams,
        action: 'aderror'
      };

    default:
      return globalParams;
  }
};
//# sourceMappingURL=handleAdsTracking.js.map