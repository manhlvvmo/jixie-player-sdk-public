export declare const styles: {
    preloadingPlaceholder: {
        backgroundColor: string;
        justifyContent: "center";
        alignItems: "center";
        position: "absolute";
    };
    thumbnail: {
        backgroundColor: string;
        justifyContent: "center";
        alignItems: "center";
    };
    playButton: {
        backgroundColor: string;
        width: number;
        height: number;
        borderRadius: number;
        justifyContent: "center";
        alignItems: "center";
    };
    playArrow: {
        color: string;
    };
    video: {
        backgroundColor?: undefined;
    } | {
        backgroundColor: string;
    };
    controls: {
        backgroundColor: string;
        height: number;
        marginTop: number;
        flexDirection: "column";
        alignItems: "center";
    };
    selectQuality: {
        position: "absolute";
        backgroundColor: string;
        height: number;
        width: number;
        right: number;
        bottom: number;
        flexDirection: "row";
    };
    width1_4: {
        width: number;
    };
    selectTextHeader: {
        color: string;
        fontSize: number;
        paddingLeft: number;
    };
    videoControls: {
        flexDirection: "row";
        justifyContent: "space-between";
        width: string;
        paddingHorizontal: number;
        paddingBottom: number;
        alignItems: "center";
    };
    playControl: {
        color: string;
    };
    backward: {
        color: string;
        paddingHorizontal: number;
    };
    forward: {
        color: string;
        padding: number;
    };
    extraControl: {
        color: string;
    };
    seekBar: {
        alignItems: "center";
        height: number;
        flexGrow: number;
        flexDirection: "row";
        paddingHorizontal: number;
    };
    seekBarFullWidth: {
        marginLeft: number;
        marginRight: number;
        paddingHorizontal: number;
        marginTop: number;
        height: number;
    };
    seekBarProgress: {
        height: number;
        backgroundColor: string;
    };
    seekBarKnob: {
        width: number;
        height: number;
        marginHorizontal: number;
        marginVertical: number;
        borderRadius: number;
        backgroundColor: string;
        transform: {
            scale: number;
        }[];
        zIndex: number;
    };
    seekBarBackground: {
        backgroundColor: string;
        height: number;
    };
    overlayButton: {
        flex: number;
    };
    activeDurationText: {
        paddingLeft: number;
        paddingRight: number;
        paddingBottom: number;
        paddingTop: number;
    };
    durationText: {
        color: string;
    };
    controlScroll: {
        paddingBottom: number;
        paddingHorizontal: number;
        maxHeight: number;
        backgroundColor: string;
    };
    qualityControlTitle: {
        fontSize: number;
        padding: number;
    };
    qualityDetail: {
        flexDirection: "row";
        alignItems: "center";
        paddingVertical: number;
    };
    backgroundControl: {
        backgroundColor: string;
    };
};
